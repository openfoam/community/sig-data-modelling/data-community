# Contributors to the OpenFOAM&reg; Data-Driven Modelling Special Interest Group Repository

The following is a list of known contributors to the [OpenFOAM&reg; Data-Driven Modelling Special Interest Group](https://wiki.openfoam.com/Data_Driven_Modelling_Special_Interest_Group) Repository.

## Contributors (alphabetical by surname)

- Data-Driven Modelling Special Interest Group members
- Mark Olesen

<!----------------------------------------------------------------------------->
